=== Login Widget With Shortcode ===


This is a simple login form in the widget. This will allow users to login to the site from frontend. 

== Description ==

* This is a simple login form in the widget. 
* just install the plugin and add the login widget in the sidebar. 
* Change some 'optional' settings in `Settings-> Login Widget Settings` and you are good to go. 
* Add css as you prefer because the form structure is really very simple.

= Other Optional Options =
* You can choose the redirect page after login.
* Choose redirect page after logout.
* Choose user profile page.

= Facebook Login Widget (PRO) =
There is a PRO version of this plugin that supports login with <strong>Facebook</strong>, <strong>Google</strong> And <strong>Twitter</strong>. You can get it <a href="http://donateafo.net84.net/fb-login-widget-pro/" target="_blank">here</a> in <strong>USD 1.00</strong>

* The PRO version also comes with a content restriction Addon. Content of Pages and Posts can be hidden from visitors of your site.
* Content Restriction can be applied from post edit page.

== Installation ==


1. Upload `login-sidebar-widget.zip` to the `/wp-content/plugins/` directory.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. Go to `Settings-> Login Widget Settings`, and set some options. It's really easy.
4. Go to `Appearance->Widgets` ,in available widgets you will find `Login Widget AFO` widget, drag it to chosen widget area where you want it to appear.
5. You can also use shortcodes to insert login form in post or pages. [login_widget title="Login Here"]
5. Now visit your blog and you will see the login form section.




== Frequently Asked Questions ==

= For any kind of problem =

1. Please email me avi.megladon@gmail.com. My site is http://avifoujdar.wordpress.com/contact/ 
2. Or you can write comments directly to my plugins page. Please visit here http://avifoujdar.wordpress.com/2014/02/13/login-widget/




== Screenshots ==

1. front end sidebar view
2. settings page view
3. widget view


== Changelog ==

= 2.0.2 =
* CSS file bug issue is solved.

= 2.0.1 =
* Shortcode functionality is added.

= 1.0.1 =
* this is the first release.


== Upgrade Notice ==

= 1.0 =
I will update this plugin when ever it is required.
