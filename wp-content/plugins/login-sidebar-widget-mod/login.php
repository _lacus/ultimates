<?php
/*
Plugin Name: Login Widget With Shortcode (mod!)
Plugin URI: http://ultimates.hu
Description: This is a simple login form in the widget. just install the plugin and add the login widget in the sidebar. Thats it. :)
Version: 2.1.2
Author: xxxavimegladon
Author URI: http://ultimates.hu
*/

include_once dirname( __FILE__ ) . '/settings.php';
include_once dirname( __FILE__ ) . '/login_afo_widget.php';
include_once dirname( __FILE__ ) . '/login_afo_widget_shortcode.php';
