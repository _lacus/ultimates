<?php

/*
Template Name: Admin ajax helper page
*/



    
    $requestIdentifier = $_POST['requestIdentifier'];
    
    switch ($requestIdentifier) {
        case "getColorImages":
            $postID = $_POST['postID'];
            $hexCode = $_POST['colorHexCode'];
            $attached_images = ProductColorClass::get_attached_images($postID);
            
            foreach ($attached_images as $key => $attached_image) {
                            if('#' . $attached_image->for_color === $hexCode){
                                $firstImage = $attached_image;
                                break;
                            }
            }
            
            ?>
<a href="<?php echo str_replace(get_option('siteurl'), '/', $firstImage->guid); ?>" id="demo1" title="magnifier" rel="gal1">  
                <img src="<?php echo str_replace(get_option('siteurl'), '/', $firstImage->image_url[0]); ?>" title="image">  
            </a> 
            <ul id="thumblist" class="clearfix">
                    <?php
                        $iteration = 0;
                        foreach ($attached_images as $key => $attached_image) {
                            if('#' . $attached_image->for_color === $hexCode){
                            ?>
                                <li>
                                    <a class="<?php if($iteration === 0){ ?>zoomThumbActive<?php } ?>" href="javascript:void(0);" rel="{gallery: 'gal1', smallimage: '<?php echo str_replace(get_option('siteurl'), '/', $attached_image->image_url[0]); ?>',largeimage: '<?php echo str_replace(get_option('siteurl'), '/', $attached_image->guid); ?>'}">
                                        <img src="<?php echo str_replace(get_option('siteurl'), '/', $attached_image->image_url[0]); ?>">
                                    </a>
                                </li>
                            <?php
                            $iteration++;
                            }
                        }
                    ?>
            </ul>
            <script type="text/javascript">
                $(document).ready(function(){  
                        $('a#demo1').jqzoom({  
                                zoomType: 'standard',  
                                lens:true,  
                                preloadImages: true,  
                                alwaysOn:false,  
                                zoomWidth: 300,  
                                zoomHeight: 250,  
                                xOffset:90,  
                                yOffset:30,  
                                position:'left' 
                        });
                        $('#thumblist li:first a').trigger('click');
                });  
            </script>
            <?php
            break;
        default:
            
            break;
    }



$current_user_caps = wp_get_current_user()->caps;

if($current_user_caps['administrator'] === TRUE){
    
    switch ($requestIdentifier) {
        case "deleteImage":
                
                $imageID = $_POST['imageID'];
                wp_delete_attachment($imageID, true);
            
            break;
        case "deleteColor":
                global $wpdb;
                $postID = $_POST['postID'];
                $colorMetaId = $_POST['colorMetaId'];
                
                $attached_images = ProductColorClass::get_attached_images($postID);
                
                $colorHex = $wpdb->get_var("SELECT meta_value FROM wp_postmeta WHERE meta_key = 'colorHexCode" . $colorMetaId . "'");
                
                foreach ($attached_images as $key => $attached_image) {
                    if('#' . $attached_image->for_color === $colorHex){
                        
                        wp_delete_attachment($attached_image->ID, true);
                        
                    }
                }
                
                delete_post_meta($postID, 'colorHexCode');
                
                $wpdb->query("DELETE FROM wp_postmeta WHERE meta_id = " . $colorMetaId);
            break;
        case "saveColorForPost":
                $postID = $_POST['postID'];
                $colorName = $_POST['colorName'];
                
                add_post_meta($postID, 'colorName', $colorName, false);
                
                $existingColorNames = $wpdb->get_results(" SELECT * 
                                                FROM  `wp_postmeta` 
                                                WHERE post_id = " . $postID . "
                                                AND meta_key = 'colorName'",
                                              OBJECT);
                
                $newMetaId = null;
                
                foreach ($existingColorNames as $key => $existingColorName) {
                    if($existingColorName->meta_value === $colorName){
                        $newMetaId = $existingColorName->meta_id;
                        break;
                    }
                }
                
                die($newMetaId);
            break;
        case "updateColor":
                $postID = $_POST['postID'];
                $colorMetaId = $_POST['colorMetaId'];
                $hexCode = $_POST['colorHexCode'];
                
                $oldColor = get_post_meta($postID, 'colorHexCode' . $colorMetaId, true);
                
                update_post_meta($postID, 'colorHexCode' . $colorMetaId, $hexCode);
                
                $attached_images = ProductColorClass::get_attached_images($postID);
                
                foreach ($attached_images as $key => $attached_image) {
                    if('#' . $attached_image->for_color === $oldColor){
                        print_r(array($attached_image->ID, 'forColor', $hexCode));
                        update_post_meta($attached_image->ID, 'forColor', $hexCode);
                    }
                }
                die();
            break;
        default:
            
            break;
    }
    
}else{
    die();
}



