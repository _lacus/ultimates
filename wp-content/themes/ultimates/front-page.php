﻿<?php get_header(); ?>
<div id="content">
	<section class="two-thirds last">
		<?php include (TEMPLATEPATH . "/parts/slider.php"); ?>
	<h2>Legújabb termékeink</h2>
		<div class="product_grid">
			<?php query_posts(
				array(
					'post_type' => 'ultimates_product',
					'showposts' => 16
				)
			);
			while (have_posts()) : the_post(); ?>
				<?php
				global $post;
				$existingColors = getExistingColorNames($post->ID);
				$sizes = getSizes($post->ID);
				?>
				<a href="<?php the_permalink() ?>" class="product" title="Szabadidő ruházat,nagyméretű ruházat,sportruházat">
					<?php if ( has_post_thumbnail() ) { ?>
						<?php the_post_thumbnail(array(300,300)); ?>
					<?php } else { ?>
						<div class="noimage"></div>
					<?php } ?>
					<strong><?php the_title(); ?></strong>
					<span class="product_popup">
						<span class="product_number"><?php the_field('identifier'); ?></span>
						
						<span class="product_sizes">
							<?php
							foreach ($sizes as $key => $size) {
								echo $size->name . (($key + 1 === sizeof($sizes)) ? '' : ', ');
							}
							?>
						</span>
						
						<span class="product_colors">
							<?php
							foreach ($existingColors as $key => $existingColor) {
							echo '<span style="background-color:' . $existingColor->hex . '"></span>';
								}
							?>
						</span>
					</span>
                                        <?php
                                            $fieldRibbon = get_field('ribbon', $post->ID);
                                            if(!empty($fieldRibbon)){
                                                ?>
                                                    <span class="ribbon"><?php echo $fieldRibbon; ?></span>
                                                <?php
                                            }
                                        ?>
                                        <?php
                                            $fieldRibbon45 = get_field('ferde_zaszlo', $post->ID);
                                            if(!empty($fieldRibbon45)){
                                                ?>
                                                    <span class="ribbon45"><?php echo $fieldRibbon45; ?></span>
                                                <?php
                                            }
                                        ?>
				</a>
                    
			<?php endwhile;
			wp_reset_query(); ?>
		</div>
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<!--h2><?php the_title(); ?></h2-->
			<?php // the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
<?php the_content(); ?>
		<?php endwhile; endif; ?>
		<?php // edit_post_link('Módosítás.', '<p>', '</p>'); ?>
		
		
	</section>
</div>
<?php
get_footer();
?>
