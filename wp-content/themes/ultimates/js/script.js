

$(document).ready(function(){
    $('div.colors > span').click(function(){
        var hexColor = $(this).css('backgroundColor');
                $.ajax({
                    url: document.location.origin + '/admin-ajax',
                    async: true,
                    type: 'POST',
                    data: {
                        requestIdentifier: 'getColorImages',
                        postID: $('#post_ID').val(),
                        colorHexCode: hexColor
                    },
                    success: function(data){
                        $('div.images').html(data.replace(/\/\//g, '/'));
                    },
                    error: function(data){
                    }
                });
    });
    
    // Open/close header login form
    $('#menu-item-18554', '#user_menu').click(function() {
        $('.login_form', 'header').toggleClass('active');
    });
});
