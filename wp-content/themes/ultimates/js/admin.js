jQuery.noConflict();
(function( $ ) {
  $(function() {
    
    $('#my_submit').click(function(e) {
        e.preventDefault();
        $('#publish').click();
    });
    
    $('ul.tabs li:eq(0) input[type=radio]').trigger('click');
    
    //http://colpick.com/plugin
    
    $('form#post').attr('enctype', 'multipart/form-data');
    
    function addNewColorClickCallback(){
        
        if($.trim($('#newColorName').val()) !== ''){
            
            $.loader({
                className:"blue-with-image",
                content:'Új szín hozzáadása...'
            });
            
            $.ajax({
                url: document.location.origin + '/admin-ajax',
                async: true,
                type: 'POST',
                data: {
                    requestIdentifier: 'saveColorForPost',
                    postID: $('#post_ID').val(),
                    colorName: $('#newColorName').val()
                },
                success: function(data) {
                    
                    var newMetaId = data;
                    
                    var currentTabNumber = $('ul.tabs li').size();
                    var nextTab = currentTabNumber + 1;
                    var newListElement = $('<li>');
                    var newHiddenMetaIdInput = $('<input>').attr({ type: 'hidden',
                                                                    value: newMetaId
                                                                    });
                    var newRadioElement = $('<input>').attr({type: 'radio', name: 'tabs', id: 'tab' + nextTab});
                    var newLabel = $('<label>').attr({for: 'tab' + nextTab});
                    $(newLabel).html($('#newColorName').val() + '<span class="tabClose">X</span>');
                    var newDiv = $('<div>').attr({id: 'tab-content' + nextTab, class: 'tab-content'});
                    
                    var newColorSelectorBlockHolder = $('<div>').attr({class: 'colorSelectorBlockHolder'});
                    var newColorSelectorBlock = $('<div>').attr({class: 'colorSelectorBlock'});
                    var newColorSelectorBlockLabel = $('<label>').attr({for: 'color_field_' + nextTab});
                    var newColorSelectorInput = $('<input>').attr({ type: 'text',
                                                                    class: 'colorPicker',
                                                                    id: 'color_field_' + nextTab,
                                                                    name: 'color_field',
                                                                    value: '',
                                                                    size: 25
                                                                    });
                    
                    var newSubmitButton = $('<input>').attr({   type: 'submit',
                                                                class: 'button button-primary button-large',
                                                                id: 'publish',
                                                                name: 'publish',
                                                                value: 'Feltöltöm',
                                                                accesskey: 'p'
                                                                });
                    
                    var newColorSelectorBr = $('<br>');
                    var newColorSelectorFileInput = $('<input>').attr({
                        type: 'file',
                        name: 'product_image_noColor_' + nextTab
                    });
                    
                    $(newColorSelectorBlockLabel).text('Szín');
                    
                    $(newColorSelectorBlock).append($(newColorSelectorBlockLabel));
                    $(newColorSelectorBlock).append($(newColorSelectorInput));
                    $(newColorSelectorBlock).append($(newColorSelectorBr));
                    $(newColorSelectorBlock).append($(newColorSelectorFileInput));
                    
                    $(newColorSelectorBlockHolder).append($(newColorSelectorBlock));
                    $(newDiv).append($(newColorSelectorBlockHolder));
                    
                    $(newDiv).append($(newSubmitButton));
                    
                    $(newListElement).append($(newHiddenMetaIdInput));
                    $(newListElement).append($(newRadioElement));
                    $(newListElement).append($(newLabel));
                    $(newListElement).append($(newDiv));
                    
                    $('ul.tabs').append($(newListElement));
                    
                    setClicks();
                    setElementNames();
                    hideLoaderMask();
                },
                error: function(data) {
                    console.log(data);
                }
            });
            
        }
        //<input type="radio" checked name="tabs" id="tab1">
        //<label for="tab1">tab 1</label>
        //<div id="tab-content1" class="tab-content animated fadeIn">
        
        
        /*
         * temporary comment
         * 
        $('.colorSelectorBlock:eq(0)').clone().appendTo($('.colorSelectorBlockHolder'));
        $('.colorSelectorBlockHolder .colorSelectorBlock:last').find('.colorPicker').val('');
        setClicks();
        setElementNames();
        */
    };
    
    function setClicks(){
        try{
            $('.colorImageList').sortable();
            $('.colorImageList').disableSelection();

            $('.addNewColor').unbind('click');
            $('#color_meta_box .tabClose').unbind('click');
            $('.colorImageHolder .delete').unbind('click');

            $('.addNewColor').click(addNewColorClickCallback);
            $('.colorPicker').colpick({
                layout:'hex',
                submit: true,
                onSubmit: function(color, hex, rgb, element){
                    $(element).val('#' + hex);
                    $(element).siblings('input[type=file]').each(function(){
                        $(this).attr('name', 'product_image_' + hex + '_0');
                    });
                    $('.colpick').hide();

                    $.loader({
                        className:"blue-with-image",
                        content:'Színkód mentése...'
                    });

                    var colorMetaId = null;

                    $(element).parents('li').find('input[type=hidden]:eq(0)').each(function(){
                        colorMetaId = $(this).val();
                    });

                    $.ajax({
                        url: document.location.origin + '/admin-ajax',
                        async: true,
                        type: 'POST',
                        data: {
                            requestIdentifier: 'updateColor',
                            postID: $('#post_ID').val(),
                            colorMetaId: colorMetaId,
                            colorHexCode: '#' + hex
                        },
                        success: function(data){
                            setElementNames();
                            hideLoaderMask();
                        },
                        error: function(data){
                            hideLoaderMask();
                            console.log(data);
                        }
                    });
                }
            });

            $('.colorImageHolder .delete').click(function(){
                var imageId = $(this).siblings('input[type=hidden]').val();
                var parentToRemove = $(this).parents('li.imageLi');

                $.loader({
                    className:"blue-with-image",
                    content:'Kép törlése...'
                });

                $.ajax({
                    url: document.location.origin + '/admin-ajax',
                    async: true,
                    type: 'POST',
                    data: {
                        requestIdentifier: 'deleteImage',
                        postID: $('#post_ID').val(),
                        imageID: imageId
                    },
                    success: function(data){
                        $(parentToRemove).remove();
                        hideLoaderMask();
                    },
                    error: function(data){
                        hideLoaderMask();
                        console.log(data);
                    }
                });

            });

            $('#color_meta_box .tabClose').click(function(){
                if(confirm('Törli a színt és a hozzá feltöltött képeket?')){
                    var colorMetaId = null;
                    var listItemToRemove = $(this).parents('li');

                    $(this).parents('li').find('input[type=hidden]:eq(0)').each(function(){
                        colorMetaId = $(this).val();
                    });

                    if(colorMetaId !== null){

                        $.loader({
                            className: "blue-with-image",
                            content: 'Szín törlése...'
                        });

                        $.ajax({
                            url: document.location.origin + '/admin-ajax',
                            async: true,
                            type: 'POST',
                            data: {
                                requestIdentifier: 'deleteColor',
                                postID: $('#post_ID').val(),
                                colorMetaId: colorMetaId
                            },
                            success: function(data) {
                                $(listItemToRemove).remove();
                                console.log(data);
                                hideLoaderMask();
                            },
                            error: function(data) {
                                hideLoaderMask();
                                console.log(data);
                            }
                        });

                    }
                }else{

                }//////delete is not confirmed
            });
        }catch(err){
            console.log(err);
        }
    }
    
    function setElementNames(){
        $('.colorSelectorBlock').each(function(index, value){
            $(this).find('label').attr('for', 'color_field_' + index);
            $(this).find('.colorPicker').attr('id', 'color_field_' + index);
            $(this).find('.color_field').attr('name', 'color_field_' + index);
            
            var colorPickerValue = '';
            
            $(this).parents('.colorSelectorBlockHolder').find('.colorPicker').each(function(){
                colorPickerValue = $(this).val();
            });
            
            if(colorPickerValue === ''){
                colorPickerValue = 'noColor';
            }
            
            $(this).find('input[type=file]').each(function(innerIndex, value){
                $(this).attr('name', 'product_image_' + colorPickerValue + '_' + innerIndex);
            });
        });
    }
    
    function setEvents(){
        
        $('.colorSelectorBlockHolder input[type=file]').change(function(){
            
        });
        
    }
    
    function hideLoaderMask(){
        jQuery('#jquery-loader-background').remove();
        jQuery('#jquery-loader').remove();
    }
    
    setElementNames();
    setClicks();
    setEvents();
  });
})(jQuery);