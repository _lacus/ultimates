

$(document).ready(function(){
    $('div.colors > span').click(function(){
        var hexColor = $(this).css('backgroundColor');
                $.ajax({
                    url: document.location.origin + '/admin-ajax',
                    async: true,
                    type: 'POST',
                    data: {
                        requestIdentifier: 'getColorImages',
                        postID: $('#post_ID').val(),
                        colorHexCode: hexColor
                    },
                    success: function(data){
                        $('div.images').html(data.replace(/\/\//g, '/'));
                    },
                    error: function(data){
                    }
                });
    });
    
    // Open/close header login form
    $('#menu-item-18554', '#user_menu').click(function() {
        $('.login_form', 'header').toggleClass('active');
    });
    
    
    for(i = 0; i < $('.wp-table-reloaded-table-name').length; i++){
        $('.wp-table-reloaded-table-name:eq(' + i + '), table.wp-table-reloaded:eq(' + i + ')').wrapAll('<div class="size-table-wrapper" />');
    }
    
    
    //$('.wp-table-reloaded-table-name:eq(0), .dataTables_wrapper:eq(0)').wrapAll('<div/>');
});


$.cssHooks.backgroundColor = {
    get: function(elem) {
        if (elem.currentStyle)
            var bg = elem.currentStyle["backgroundColor"];
        else if (window.getComputedStyle)
            var bg = document.defaultView.getComputedStyle(elem,
                null).getPropertyValue("background-color");
        if (bg.search("rgb") === -1)
            return bg;
        else {
            bg = bg.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }
            return "#" + hex(bg[1]) + hex(bg[2]) + hex(bg[3]);
        }
    }
};

