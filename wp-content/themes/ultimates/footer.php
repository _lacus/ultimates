	</div>
	<footer id="footer" role="contentinfo">
		<div class="middle">
			<nav id="footer_menu" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
			<div class="search-box">
				<?php get_search_form(); ?>
			</div>
			<nav id="footer_social_menu" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
			<p id="copy_text">© Copyright - Ultimates <?php echo date("Y"); ?> | Szabadidő ruházat, nagyméretű ruházat, sportruházat 1992 óta.</p>
		</div>
	</footer>
<?php wp_footer(); ?>
</body>
</html>
