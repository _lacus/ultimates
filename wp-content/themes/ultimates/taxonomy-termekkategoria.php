<?php get_header(); ?>
<div id="content">
	<section class="two-thirds last">
            <div class="breadcrumbs">
                    <?php if(function_exists('bcn_display')) bcn_display(); ?>
            </div>
            <?php
            $queried_object = get_queried_object(); 
            ?>
            <h2><?php echo $queried_object->name;?></h2>
            
            <?php if(function_exists('wp_page_numbers')) { wp_page_numbers(); } ?>
            <div class="product_grid">
                <?php while (have_posts()) : the_post(); ?>
                    <?php
                    global $post;
                    $existingColors = getExistingColorNames($post->ID);
                    $sizes = getSizes($post->ID);
                    ?>
                    <a href="<?php the_permalink() ?>" class="product">
                        <?php if (has_post_thumbnail()) { ?>
                            <?php the_post_thumbnail(array(300, 300)); ?>
                        <?php } else { ?>
                            <div class="noimage"></div>
                        <?php } ?>
                        <strong><?php the_title(); ?></strong>
                        <span class="product_popup">
                            <span class="product_number"><?php the_field('identifier'); ?></span>

                            <span class="product_sizes">
                                <?php
                                foreach ($sizes as $key => $size) {
                                    echo $size->name . (($key + 1 === sizeof($sizes)) ? '' : ', ');
                                }
                                ?>
                            </span>

                            <span class="product_colors">
                                <?php
                                foreach ($existingColors as $key => $existingColor) {
                                    echo '<span style="background-color:' . $existingColor->hex . '"></span>';
                                }
                                ?>
                            </span>

                        </span>
                        <?php
                            $fieldRibbon = get_field('ribbon', $post->ID);
                            if(!empty($fieldRibbon)){
                                ?>
                                    <span class="ribbon"><?php echo $fieldRibbon; ?></span>
                                <?php
                            }
                        ?>
                        <?php
                            $fieldRibbon45 = get_field('ferde_zaszlo', $post->ID);
                            if(!empty($fieldRibbon45)){
                                ?>
                                    <span class="ribbon45"><?php echo $fieldRibbon45; ?></span>
                                <?php
                            }
                        ?>
                    </a>
                <?php endwhile; ?>
            </div>
            <?php if(function_exists('wp_page_numbers')) { wp_page_numbers(); } ?>
            
            
            
		<?php edit_post_link('Módosítás.', '<p>', '</p>'); ?>
	</section>
</div>
<?php
get_template_part( "sidebar", "parents" );
get_footer();
?>
