<?php get_header(); ?>

<div id="content" class="content holder">
	<div class="main">
		<h1 class="archive-title">Keresési találatok:</h1>
		
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="search_resoult">
					<h2><a href="<?php echo get_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
					<p><a href="<?php echo get_permalink() ?>" rel="bookmark" title="<?php the_title(); ?>">Tovább a keresési találatra</a></p>
				</div>
			<?php endwhile; ?>
			<div class="pagination">
				<?php if(function_exists('wp_page_numbers')) { wp_page_numbers(); } ?>
			</div>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
