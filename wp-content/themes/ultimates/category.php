<?php get_header(); ?>
<div id="content">
	<section class="two-thirds last article_list">
		<div class="breadcrumbs">
			<?php if(function_exists('bcn_display')) bcn_display(); ?>
		</div>
		<h2 class="archive-title"><?php echo single_cat_title(); ?></h2>
		<?php if ( have_posts() ) : ?>
			<?php while ( have_posts() ) : the_post(); ?>
			<div class="article">
				<?php if ( has_post_thumbnail() ) { ?>
					<a href="<?php echo get_permalink() ?>" class="img" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_post_thumbnail(array(120,120)); ?></a>
				<?php } else { ?>
					<a href="<?php echo get_permalink() ?>" class="noimage" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"></a>
				<?php } ?>
				<?php $title = get_the_title(); ?>
				<h3 class="<?php if(strlen($title = 0)) print("hidden") ?>"><a href="<?php echo get_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>"><?php the_title(); ?></a></h3>
				<?php the_excerpt_rereloaded('29','Olvasd el a cikket','','div','more','no'); ?>
			</div>
			<?php endwhile; ?>
			<div class="pagination">
				<?php if(function_exists('wp_page_numbers')) { wp_page_numbers(); } ?>
			</div>
		<?php else : ?>
		<?php endif; ?>
	</section>
</div>
<?php
//get_sidebar();
get_footer();
?>
