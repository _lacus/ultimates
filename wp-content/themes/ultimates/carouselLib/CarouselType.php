<?php

add_action( 'init', 'createCarouselType' );

function createCarouselType() {
	register_post_type( 'carousel',
		array(
			'labels' => array(
				'name' => __( 'Carousel' ),
				'singular_name' => __( 'Carousel Post' )
			),
		'public' => true,
		'has_archive' => true,
                'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
		)
	);
}

//---------- Init taxonomies -------------


