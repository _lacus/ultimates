<?php get_header(); ?>
<div id="content">
	<section class="two-thirds last">
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
			<h2>404</h2>
			<p>Sajnos a megadott tartalom nem található!</p>
	</section>
</div>
<?php
get_footer();
?>
