<?php get_header(); ?>
<div id="content">
	<section class="two-thirds last">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
			<h2><?php the_title(); ?></h2>
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>
		<?php endwhile; endif; ?>
		<?php edit_post_link('Módosítás.', '<p>', '</p>'); ?>
	</section>
</div>
<?php
get_template_part( "sidebar", "parents" );
get_footer();
?>
