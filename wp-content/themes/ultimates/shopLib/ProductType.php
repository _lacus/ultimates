<?php

add_action( 'init', 'createProductType' );

function createProductType() {
	register_post_type( 'ultimates_product',
		array(
			'labels' => array(
				'name' => __( 'Termékek' ),
				'singular_name' => __( 'Termék' )
			),
		'public' => true,
		'has_archive' => true,
                'supports' => array('title', 'editor', 'thumbnail', 'excerpt')
		)
	);
}

//---------- Init taxonomies -------------

add_action( 'init', 'initProductTaxonomy' );

function initProductTaxonomy() {
	register_taxonomy(
		'termekkategoria',
		'ultimates_product',
		array(
			'label' => __( 'Termékkategória' ),
			'rewrite' => array( 'slug' => 'termekkategoria' ),
                        'hierarchical' => true
		)
	);
}

add_action( 'init', 'initSizeTaxonomy' );

function initSizeTaxonomy() {
	register_taxonomy(
		'meret',
		'ultimates_product',
		array(
			'label' => __( 'Méret' ),
			'rewrite' => array( 'slug' => 'meret' ),
                        'hierarchical' => true
		)
	);
}

add_action( 'init', 'initSeasonTaxonomy' );

function initSeasonTaxonomy() {
	register_taxonomy(
		'season',
		'ultimates_product',
		array(
			'label' => __( 'Idény' ),
			'rewrite' => array( 'slug' => 'ideny' ),
                        'hierarchical' => true
		)
	);
}


add_action( 'init', 'initPositionTaxonomy' );

function initPositionTaxonomy() {
	register_taxonomy(
		'position',
		'ultimates_product',
		array(
			'label' => __( 'Pozíció' ),
			'rewrite' => array( 'slug' => 'pozicio' ),
                        'hierarchical' => true
		)
	);
}
