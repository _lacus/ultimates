<?php


/**
 * Calls the class on the post edit screen.
 */
function call_ProductColorClass() {
    new ProductColorClass();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_ProductColorClass' );
    add_action( 'load-post-new.php', 'call_ProductColorClass' );
}

/** 
 * The ProductColorClass.
 */
class ProductColorClass {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
	}
        
        public static function get_attached_images($post_id){
            $attached_images = get_attached_media('image', $post_id);
            
            foreach ($attached_images as $key => $attached_image) {
                $image_id = $attached_image->ID;
                $for_color = get_post_meta($image_id, 'forColor', true);
                $image_url = wp_get_attachment_image_src($image_id, 'product', false);
                $attached_images[$key]->for_color = str_replace('#', '', $for_color);
                $attached_images[$key]->image_url = $image_url;
            }
            
            $storedOrder = get_post_meta($post_id, 'imageOrder');
            $storedOrderArray = json_decode($storedOrder[0]);
            
            if($storedOrder){            
                foreach ($storedOrderArray as $key => $order) {
                    $attached_images = self::sortArrayByArray($attached_images, $order);
                }
            }
            
            return $attached_images;
        }
        
        public static function sortArrayByArray(Array $array, Array $orderArray) {
            $ordered = array();
            foreach($orderArray as $key) {
                if(array_key_exists($key,$array)) {
                    $ordered[$key] = $array[$key];
                    unset($array[$key]);
                }
            }
            return $ordered + $array;
        }
        
	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('ultimates_product');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'color_meta_box'
			,__( 'Termék Színek Hozzáadása', 'ultimates_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);
            }
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                $postType = get_post_type($post_id);
                if($postType === 'ultimates_product'){
                    $uploads = wp_upload_dir();
                    global $current_user;
                    get_currentuserinfo();
                    $logged_in_user = $current_user->ID;
                    
                    $attached_images = $this->get_attached_images($post_id);
                    
                    storeImageOrder();
                    
                    foreach ($_FILES as $key => $file) {
                        if(false !== strpos($key, 'product_image') && $file['name'] !== ''){
                            $uploadData = media_handle_upload($key, $post_id);
                            
                            $keyArray = explode('_', $key);
                            $hex = $keyArray[2];
                            add_post_meta($uploadData, 'forColor', $hex, true);
                            
                            if(sizeof($attached_images) > 0){
                                foreach ($attached_images as $key_ => $attached_image) {
                                    if($attached_image->for_color === $hex){
                                        wp_delete_attachment($attached_image->ID, true);
                                    }
                                }
                            }
                        }
                    }

                    /*
                     * We need to verify this came from the our screen and with proper authorization,
                     * because save_post can be triggered at other times.
                     */

                    // Check if our nonce is set.
                    if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
                            return $post_id;

                    $nonce = $_POST['myplugin_inner_custom_box_nonce'];

                    // Verify that the nonce is valid.
                    if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
                            return $post_id;

                    // If this is an autosave, our form has not been submitted,
                    //     so we don't want to do anything.
                    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                            return $post_id;

                    // Check the user's permissions.
                    if ( 'page' == $_POST['post_type'] ) {

                            if ( ! current_user_can( 'edit_page', $post_id ) )
                                    return $post_id;

                    } else {

                            if ( ! current_user_can( 'edit_post', $post_id ) )
                                    return $post_id;
                    }

                    /* OK, its safe for us to save the data now. */

                    // Sanitize the user input.
                    $mydata = sanitize_text_field( $_POST['myplugin_new_field'] );

                    // Update the meta field.
                    update_post_meta( $post_id, '_my_meta_value_key', $mydata );
                }
	}

        
	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
                global $wpdb;
                
                echo '<input type="text" value="" name="newColorName" id="newColorName">';
                echo '<br/><input type="button" class="addNewColor" value="Új Szín Hozzáadása"/>';
                
                $existingColorNames = $wpdb->get_results(" SELECT * 
                                                FROM  `wp_postmeta` 
                                                WHERE post_id = " . $post->ID . "
                                                AND meta_key = 'colorName'",
                                              OBJECT);
                
                ?>
                <ul class="tabs">
                    <?php
                        foreach ($existingColorNames as $key => $existingColorName) {
                            $colorHex = $wpdb->get_var("SELECT meta_value FROM wp_postmeta WHERE meta_key = 'colorHexCode" . $existingColorName->meta_id . "'");
                            ?>
                                <li>
                                    <input type="hidden" value="<?=$existingColorName->meta_id?>" />
                                    <input type="radio" <?php if($key===0){ ?>checked<?php } ?> name="tabs" id="tab<?=($key + 1)?>">
                                    <label for="tab<?=($key + 1)?>"><?=$existingColorName->meta_value?><span class="tabClose">X</span></label>
                                    <div id="tab-content<?=($key + 1)?>" class="tab-content animated fadeIn">
                                    <?php
                                        $attached_images = $this->get_attached_images($post->ID);
                                        
                                        // Add an nonce field so we can check for it later.
                                        wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
                                        
                                        // Use get_post_meta to retrieve an existing value from the database.
                                        $value = get_post_meta( $post->ID, '_my_meta_value_key', true );

                                        // Display the form, using the current value.
                                        echo '<div class="colorSelectorBlockHolder">';
                                        
                                        $noSuitableFound = true;
                                        
                                        if(sizeof($attached_images) > 0){
                                            ?>
                                            <ul class="colorImageList">
                                            <?php
                                            $iteration = 0;
                                            foreach ($attached_images as $key => $attached_image) {
                                                if('#' . $attached_image->for_color === $colorHex){
                                                    ?>
                                                    <li class="imageLi">
                                                    <?php
                                                    $noSuitableFound = false;
                                                    echo '<div class="colorSelectorBlock">';
                                                    echo '<div class="colorImageHolder">';
                                                    echo '<input type="hidden" name="color_image_' . $colorHex . '_' . $iteration . '" value="' . $attached_image->ID . '" />';
                                                    echo '<img src="' . $attached_image->image_url[0] . '" />';
                                                    echo '<span class="delete">X</span></div>';
                                                    echo '</div>';
                                                    $iteration++;
                                                    ?>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            </ul>
                                            <?php
                                        }
                                        
                                        
                                        echo '<div class="colorSelectorBlock"><label for="color_field">';
                                        _e( 'Szín', 'ultimates_textdomain' );
                                        echo '</label> ';
                                        echo '<input type="text" class="colorPicker" id="color_field" name="color_field" value="' . $wpdb->get_var("SELECT meta_value FROM wp_postmeta WHERE meta_key = 'colorHexCode" . $existingColorName->meta_id . "'") . '"';
                                        echo ' value="" size="25" />';
                                        echo '<br /><input type="file" name="product_image_noColor_0" />';
                                        echo '</div>';
                                        echo '</div>';
                                        echo '<input type="submit" name="publish" id="publish" class="button button-primary button-large" value="Feltöltöm" accesskey="p">';
                                    ?>
                                    </div>
                                </li>
                            <?php
                        }
                    ?>
                </ul>
                <?php
	}
}