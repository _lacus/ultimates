<?php

/**
 * Calls the class on the post edit screen.
 */
function call_RetailerClass() {
    new RetailerClass();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_RetailerClass' );
    add_action( 'load-post-new.php', 'call_RetailerClass' );
}

/** 
 * The RetailerClass.
 */
class RetailerClass {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
	}
        
	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('viszonteladok');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'retailer_meta_box'
			,__( 'Viszonteladó adatai', 'ultimates_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);
            }
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                $postType = get_post_type($post_id);
                if($postType === 'viszonteladok'){
                    
                    /*
                     * We need to verify this came from the our screen and with proper authorization,
                     * because save_post can be triggered at other times.
                     */

                    // Check if our nonce is set.
                    if ( ! isset( $_POST['retailer_inner_custom_box_nonce'] ) )
                            return $post_id;

                    $nonce = $_POST['retailer_inner_custom_box_nonce'];

                    // Verify that the nonce is valid.
                    if ( ! wp_verify_nonce( $nonce, 'retailer_inner_custom_box' ) )
                            return $post_id;
                    
                    // If this is an autosave, our form has not been submitted,
                    //     so we don't want to do anything.
                    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
                            return $post_id;
                    
                    // Check the user's permissions.
                    if ( 'page' == $_POST['post_type'] ) {

                            if ( ! current_user_can( 'edit_page', $post_id ) )
                                    return $post_id;

                    } else {

                            if ( ! current_user_can( 'edit_post', $post_id ) )
                                    return $post_id;
                    }
                    
                    /* OK, its safe for us to save the data now. */
                    
                    
                    
                    


            
                    
                    
                    
                    
                    //Sanitize the user input.
                    $address = sanitize_text_field($_POST['retailer_address']);
                    $phone = sanitize_text_field($_POST['retailer_phone']);
                    $email = sanitize_text_field($_POST['retailer_email']);
                    $weburl = sanitize_text_field($_POST['retailer_weburl']);
                    //$geoCodes = $this->lookup($address);
                    
                    // Update the meta field.
                    update_post_meta($post_id, 'retailer_address', $address);
                    update_post_meta($post_id, 'retailer_phone', $phone);
                    update_post_meta($post_id, 'retailer_email', $email);
                    update_post_meta($post_id, 'retailer_weburl', $weburl);
                    update_post_meta($post_id, 'latitude', $_POST['retailer_latitude']);
                    update_post_meta($post_id, 'longitude', $_POST['retailer_longitude']);
                    
                }
	}

        
	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
                wp_nonce_field( 'retailer_inner_custom_box', 'retailer_inner_custom_box_nonce' );
                ?>
                <p>
                    
                </p>
                <p>
                    <label for="retailer_address">Cím</label>
                    <input type="text" name="retailer_address" id="retailer_address" value="<?=get_post_meta($post->ID, 'retailer_address', true)?>" />
                </p>
                <p>
                    <label for="retailer_phone">Telefonszám</label>
                    <input type="text" name="retailer_phone" id="retailer_phone" value="<?=get_post_meta($post->ID, 'retailer_phone', true)?>" />
                </p>
                <p>
                    <label for="retailer_email">E-mail cím</label>
                    <input type="text" name="retailer_email" id="retailer_email" value="<?=get_post_meta($post->ID, 'retailer_email', true)?>" />
                </p>
                <p>
                    <label for="retailer_weburl">Web</label>
                    <input type="text" name="retailer_weburl" id="retailer_weburl" value="<?=get_post_meta($post->ID, 'retailer_weburl', true)?>" />
                </p>
                <p>
                    <label for="retailer_latitude">Latitude</label>
                    <input type="text" name="retailer_latitude" id="retailer_latitude" value="<?=get_post_meta($post->ID, 'latitude', true)?>" />
                </p>
                <p>
                    <label for="retailer_longitude">Longitude</label>
                    <input type="text" name="retailer_longitude" id="retailer_longitude" value="<?=get_post_meta($post->ID, 'longitude', true)?>" />
                </p>
                <p>
                    <a href="http://www.latlong.net/convert-address-to-lat-long.html" target="_blank">Geocoder</a>
                </p>
                <input type="submit" id="my_submit" class="button button-primary button-large" value="Mentés" accesskey="p">
                <?php
	}
        
        function lookup($string) {
            
            require 'maps/GoogleMapAPI-2.5/GoogleMapAPI.class.php';
            
            $map = new GoogleMapAPI('map');
 
            $apiKey = 'AIzaSyANOYvvp3QXEhrwquMsqgl31H6CALbefMQ';
            
            // enter YOUR Google Map Key
            $map->setAPIKey($apiKey);
            
            $location = $map->getGeocode($string);
            
            print_r($location);
            
            die();
            
            return $array;
        }
        
}

