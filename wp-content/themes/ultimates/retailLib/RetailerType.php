<?php

add_action( 'init', 'createRetailerType' );

function createRetailerType() {
	register_post_type( 'viszonteladok',
		array(
			'labels' => array(
				'name' => __( 'Viszonteladók' ),
				'singular_name' => __( 'Viszonteladó' )
			),
		'public' => true,
		'has_archive' => true,
		)
	);
}

//---------- Init taxonomies -------------

add_action( 'init', 'initRetailerTaxonomy' );

function initRetailerTaxonomy() {
	register_taxonomy(
		'termekmeret',
		'viszonteladok',
		array(
			'label' => __( 'Termékméret' ),
			'rewrite' => array( 'slug' => 'termekmeret' ),
                        'hierarchical' => true
		)
	);
}
