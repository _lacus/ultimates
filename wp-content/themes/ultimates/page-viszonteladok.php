<?php 
    /*
    Template Name: Viszonteladok
    */
    get_header();
?>
<div id="content">
	<section class="two-thirds last">
            <div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
            <div id="map">
                
            </div>
            <script type="text/javascript">
                function initialize(){
                    var mapProp = {
                        center:new google.maps.LatLng(47.498405600000000000, 19.040757799999938000),
                        zoom:6,
                        mapTypeId:google.maps.MapTypeId.ROADMAP
                    };
                    var googleMap = new google.maps.Map(document.getElementById("map"), mapProp);
                    
                    var markers = [];
                    jQuery('.geoSpan').each(function(){
                        var geoCodes = $(this).text().split(',');
                        var marker = new google.maps.Marker({
                            map: googleMap,
                            position: new google.maps.LatLng(Number(geoCodes[0]), Number(geoCodes[1])),
                            optimized: false,
                            zIndex: 6
                        });
                        
                        console.log($(this).parents('li').find('span.title').text());
                        
                        var infowindow = new google.maps.InfoWindow({
                                content: '<b>' + $(this).parents('li').find('span.title').text() + '</b><br /><br />' + $(this).parents('li').find('span.address').text(),
                                size: new google.maps.Size(300, 300),
                                width: 200
                        });
                        
                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.open(googleMap, marker);
                        });
                        
                        markers.push(marker);
                        console.log(markers);
                    });
                }
                google.maps.event.addDomListener(window, 'load', initialize);
            </script>
            
            <?php
                $type = 'viszonteladok';
                $args = array(
                    'post_type' => $type,
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                );
                
                $my_query = new WP_Query($args);
                if ($my_query->have_posts()) {
                    ?>
                    <h2>Normál méretet forgalmazó viszonteladók</h2>
                    <ul class="shop">
                    <?php
                    while ($my_query->have_posts()) : $my_query->the_post();
                        global $post;
                        $terms = wp_get_post_terms($post->ID, 'termekmeret');
                        $location = get_retailer_location($post->ID);
                        
                        $canListed = false;
                        foreach ($terms as $key => $term) {
                            if($term->term_id === 22){
                                $canListed = true;
                                break;
                            }
                        }
                        if($canListed){
                            $extraInfo = getRetailerInfos($post->ID);
                        ?>
                        <li>
                            <span class="title"><?php echo $post->post_title; ?></span>
                            <span class="meta">
		                        <?php
		                            if(!empty($extraInfo['address'])){
		                        ?>
		                        <span class="address"><?=$extraInfo['address']?></span>
		                        <?php
		                            }
		                        ?>
		                        <?php
		                            if(!empty($extraInfo['phone'])){
		                        ?>
		                        <span class="phone"><?=$extraInfo['phone']?></span>
		                        <?php
		                            }
		                        ?>
		                        <?php
		                            if(!empty($extraInfo['email'])){
		                        ?>
		                        <span class="email"><a href="mailto:<?=$extraInfo['email']?>"><?=$extraInfo['email']?></a></span>
		                        <?php
		                            }
		                        ?>
		                        <?php
		                            if(!empty($extraInfo['web'])){
		                        ?>
		                        <span class="web"><a href="<?=$extraInfo['web']?>"><?=$extraInfo['web']?></a></span>
		                        <?php
		                            }
		                        ?>
		                        <span class="geoSpan"><?php echo implode(',', $location); ?></span>
		                    </span>
                        </li>
                        <?php
                        }
                    endwhile;
                    ?>
                    </ul>
                    <h2>Extra méretet forgalmazó viszonteladók</h2>
                    <ul class="shop">
                    <?php
                    while ($my_query->have_posts()) : $my_query->the_post();
                        global $post;
                        $location = get_retailer_location($post->ID);
                        $terms = wp_get_post_terms($post->ID, 'termekmeret');
                        $canListed = false;
                        foreach ($terms as $key => $term) {
                            if($term->term_id === 23){
                                $canListed = true;
                                break;
                            }
                        }
                        if($canListed){
                            $extraInfo = getRetailerInfos($post->ID);
                        ?>
                        <li>
                            <span class="title"><?php echo $post->post_title; ?></span>
                            <span class="meta">
		                        <?php
		                            if(!empty($extraInfo['address'])){
		                        ?>
		                        <span class="address"><?=$extraInfo['address']?></span>
		                        <?php
		                            }
		                        ?>
		                        <?php
		                            if(!empty($extraInfo['phone'])){
		                        ?>
		                        <span class="phone"><?=$extraInfo['phone']?></span>
		                        <?php
		                            }
		                        ?>
		                        <?php
		                            if(!empty($extraInfo['email'])){
		                        ?>
		                        <span class="email"><a href="mailto:<?=$extraInfo['email']?>"><?=$extraInfo['email']?></a></span>
		                        <?php
		                            }
		                        ?>
		                        <?php
		                            if(!empty($extraInfo['web'])){
		                        ?>
		                        <span class="web"><a href="<?=$extraInfo['web']?>"><?=$extraInfo['web']?></a></span>
		                        <?php
		                            }
		                        ?>
		                        <span class="geoSpan"><?php echo implode(',', $location); ?></span>
		                    </span>
                        </li>
                        <?php
                        }
                    endwhile;
                    ?>
                    </ul>
                    <?php
                }
                wp_reset_query();  // Restore global post data stomped by the_post().
            ?>
	</section>
</div>
<?php
get_template_part( "sidebar", "parents" );
get_footer();
?>
