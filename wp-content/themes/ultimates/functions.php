<?php

include_once 'shopLib/includes.php';
include_once 'retailLib/includes.php';
include_once 'carouselLib/includes.php';

/*JQUERY repair*/
if ( !is_admin() ) {
	function changejquery() {
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
		wp_enqueue_script( 'jquery' );
	}
	add_action('init', 'changejquery');
}

function addAdminScript() {
    wp_enqueue_script( 'colpickJs', get_bloginfo('template_directory') . '/js/colpick.js' );
    wp_enqueue_script( 'adminJs', get_bloginfo('template_directory') . '/js/admin.js' );
    wp_enqueue_script('loaderJs', get_bloginfo('template_directory') . '/js/mimiz-jquery-loader-plugin-200d6e6/jquery.loader.js');
}
add_action('admin_enqueue_scripts', 'addAdminScript');

function customAdminCss() {
        echo '<link href="' . get_bloginfo('template_directory') . '/css/admin.css' . '" rel="stylesheet" type="text/css">';
        echo '<link href="' . get_bloginfo('template_directory') . '/css/colpick.css' . '" rel="stylesheet" type="text/css">';
        echo '<link href="' . get_bloginfo('template_directory') . '/js/mimiz-jquery-loader-plugin-200d6e6/jquery.loader.css' . '" rel="stylesheet" type="text/css">';
}
add_action('admin_head', 'customAdminCss');

//echo plugin_dir_url( __FILE__ ) . 'js/admin.js' . '<br/>';
// Disable theme editor and plugin editor
//define( 'DISALLOW_FILE_EDIT', true );
//define( 'DISALLOW_FILE_MODS', true );

// MENU SETTINGS

// Register Menus
register_nav_menu( 'primary', __( 'Navigation Menu', 'tevezzokosan' ) );
register_nav_menu( 'sidebar', __( 'Sidebar menu', 'tevezzokosan' ) );
register_nav_menu( 'footer', __( 'Footer menu', 'tevezzokosan' ) );
register_nav_menu( 'user', __( 'User menu', 'tevezzokosan' ) );

// First/last menu class
function add_first_and_last($output) {
	$output = preg_replace('/class="menu-item/', 'class="first-menu-item menu-item', $output, 1);
	$output = substr_replace($output, 'class="last-menu-item menu-item', strripos($output, 'class="menu-item'), strlen('class="menu-item'));
	return $output;
}
add_filter('wp_nav_menu', 'add_first_and_last');

// CONTENT

/*excrept rereloaded*/
function the_excerpt_rereloaded($words = 40, $link_text = 'Continue reading this entry &#187;', $allowed_tags = '', $container = 'p', $atagclass = 'morea', $smileys = 'no', $need_more_button = true , $return = false) {
	global $post;
	
	if ( $allowed_tags == 'all' ) 
		$allowed_tags = '<a>,<i>,<em>,<b>,<strong>,<ul>,<ol>,<li>,<span>,<blockquote>,<img>';
	
	$text = preg_replace('/\[.*\]/', '', strip_tags($post->post_content, $allowed_tags));
	
	$text = explode(' ', $text);
	$tot = count($text);
	
	for ( $i=0; $i<$words; $i++ ) :
		$output .= $text[$i] . ' ';
	endfor;
		
	if ( $smileys == "yes" ) 
		$output = convert_smilies($output);
	
	$data = '<p>'.force_balance_tags($output);
	
	//ha a szöveg hosszabb, mint amennyi szót meg akarunk jeleníteni, akkor '...', különben lezáró </p>
	if ( $i < $tot ) :
		$data .= '...';
	else :
		$data .= '</p>';
	endif;
	
	//if ( $i < $tot ) : <-- mindig kell a "more"
		if ( $container == 'p' || $container == 'div' ) : $data .= '</p>'; endif;
		if ( $container != 'plain' ) : $data .= '<'.$container.' class="more">';
			if ( $container == 'div' ) : $data .= '<p>'; endif;
		endif;
	
		//$need_more_button alapértelmezetten true, tehát kiírja a gombot is
		if ($need_more_button) { 
			$data .= '<a class="'.$atagclass.'" href="'.get_permalink().'" title="'.$link_text.'">'.$link_text.'</a>';
		}
	
		if ( $container == 'div' ) : $data .= '</p>'; endif;
		if ( $container != 'plain' ) : $data .= '</'.$container.'>'; endif;
		if ( $container == 'plain' || $container == 'span' ) : $data .= '</p>'; endif; 
		//endif;
	
	// $return alapértelmezetten false, tehát kiírja a változó tartalmát
	if ($return)
		return $data;
	else
		echo $data;
}

add_theme_support( 'post-thumbnails', array('post', 'page', 'ultimates_product', 'carousel') );

function getFuturePostsByCategoryId($categoryId){
    $query = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('future'),
        'orderby' => 'post_date',
        'order' => 'ASC'
        ));
    
    return $query;    
}

function getPostsOnDateByCategoryId($timestamp, $categoryId){
    //parent 485 type
    //parent 486 channel
    $query = new WP_Query( $args = array(
        'date_query' => array(
                                'year' => date('Y', $timestamp),
                                'month' => date('m', $timestamp),
                                'day'   => date('d', $timestamp)
                             ),
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'ASC'
        ));
    
    $sameDateTimeChecker = array();
    
    while($query->have_posts()){
        $query->next_post();        
        $post_categories = wp_get_post_categories($query->post->ID);
        foreach($post_categories as $c){
                $cat = get_category( $c );
                if($cat->parent === 485){
                    $query->post->customCategory = $cat->name;
                }
                if($cat->parent === 486){
                    $query->post->channel = $cat->name;
                }
        }
        $sameDateTimeChecker[] = $query->post->post_date . $query->post->post_title;
        
    }
    
    
    return $query;
}

function getFirstAndLastPostDateInCategory($categoryId){
    
    $intervalArray = array();
    
    $query = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'ASC',
        'posts_per_page' => 1
        ));
    
    
    while($query->have_posts()){
        $intervalArray['firstDate'] = $query->post->post_date;
        $query->next_post();
    }
    
    $query2 = new WP_Query( $args = array(
        'cat' => $categoryId,
        'post_status' => array('publish', 'future'),
        'orderby' => 'post_date',
        'order' => 'DESC',
        'posts_per_page' => 1
        ));
    
    
    while($query2->have_posts()){
        $intervalArray['lastDate'] = $query2->post->post_date;
        $query2->next_post();
    }
    
    return $intervalArray;
    
}

function getSuggestionsUrl($diff){
    $actual_link = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'].'/?page_id='.$_GET['page_id'];
    
    $baseDate = date('Y-m-d', strtotime('now'));
    
    if(empty($_GET['datum'])){
        $generatedDate = date('Y-m-d', strtotime($baseDate . $diff));
    }else{
        $baseDate = $_GET['datum'];
        $generatedDate = date('Y-m-d', strtotime($baseDate . $diff));
    }
    
    $generatedUrl = $actual_link . '&datum=' . $generatedDate;
    return $generatedUrl;
}

function getExistingColorNames($postID){
    global $wpdb;
    $existingColorNames = $wpdb->get_results(" SELECT * 
                                                FROM  `wp_postmeta` 
                                                WHERE post_id = " . $postID . "
                                                AND meta_key = 'colorName'",
                                              OBJECT);
    
    foreach($existingColorNames as $key => $existingColorName){
        $hexcode = $wpdb->get_var("SELECT meta_value FROM wp_postmeta WHERE meta_key = 'colorHexCode" . $existingColorName->meta_id . "'");
        $existingColorNames[$key]->hex = $hexcode;
    }
    
    return $existingColorNames;
}

function getSizes($postId){
    $sizes = wp_get_post_terms($postId, 'meret');
    
    usort($sizes, function($a, $b){
        $sizeOrder = array(
            'S',
            'M',
            'L',
            'XL',
            'XXL',
            '3XL',
            '4XL',
            '5XL',
            '6XL',
            '7XL',
            '8XL'
        );
        if(array_search($a->name, $sizeOrder) < array_search($b->name, $sizeOrder)){
            return -1;
        }else{
            return 1;
        }
    });
    
    return $sizes;
}

function getRetailerInfos($postId){
    $infos = array();
    
    $infos['address'] = get_post_meta($postId, 'retailer_address', true);
    $infos['phone'] = get_post_meta($postId, 'retailer_phone', true);
    $infos['email'] = get_post_meta($postId, 'retailer_email', true);
    $infos['web'] = get_post_meta($postId, 'retailer_weburl', true);
    
    return $infos;
}

function get_retailer_location($retailerId){
    $location = array();
    
    $location['latitude'] = get_post_meta($retailerId, 'latitude', true);
    $location['longitude'] = get_post_meta($retailerId, 'longitude', true);
    
    return $location;
}

function get_custom_post_type_template($single_template) {
     global $post;
     if($post->post_type == 'ultimates_product'){
          $single_template = dirname( __FILE__ ) . '/ultimates-product.php';
     }
     return $single_template;
}

function storeImageOrder(){
    $imageOrder = array();
    foreach ($_POST as $key => $postValue) {
        if(strpos($key, 'color_image_') !== FALSE){
            $keyArray = explode('_', $key);
            $colorHex = $keyArray[2];
            $imageOrder[$colorHex][] = $postValue;
        }
    }
    $postID = $_POST['post_ID'];
    
    update_post_meta($postID, 'imageOrder', json_encode($imageOrder));
    
}
 
add_filter( "single_template", "get_custom_post_type_template" ) ;

add_image_size( 'product', 450, 504, false );

add_action( 'init', 'createCurrencyType' );

function createCurrencyType() {
	register_post_type( 'penznemek',
		array(
			'labels' => array(
				'name' => __( 'Pénznemek' ),
				'singular_name' => __( 'Pénznem' )
			),
		'public' => true,
		'has_archive' => true,
		)
	);
}

add_theme_support( 'custom-background' );
