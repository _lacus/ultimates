<?php get_header(); ?>
<div id="content">
	<section class="two-thirds last">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                        <?php
                            global $post;
                            $existingColors = getExistingColorNames($post->ID);
                            $sizes = getSizes($post->ID);
                        ?>
                        <input type='hidden' id='post_ID' name='post_ID' value='<?=$post->ID?>' />
			<div class="breadcrumbs">
				<?php if(function_exists('bcn_display')) bcn_display(); ?>
			</div>
			
			<h2><?php the_title(); ?></h2>
			
			<div class="product_left">
				<div class="images">
                            <?php
                                $firstColor = $existingColors[0];
                                $hexCode = $firstColor->hex;
                                $attached_images = ProductColorClass::get_attached_images($post->ID);
                                
                                $firstImage = null;
                                
                                foreach($attached_images as $xkey => $fimage){
                                    $firstImage = $fimage;
                                    break;
                                }
                                
                            ?>
                            <a href="<?php echo $firstImage->guid; ?>" id="demo1" title="magnifier" rel="gal1">  
                                <img src="<?php echo $firstImage->image_url[0]; ?>" title="image">  
                            </a> 
                            
                            <ul id="thumblist" class="clearfix">
                                    <?php
                                        $iteration = 0;
                                        foreach ($attached_images as $key => $attached_image) {
                                            if('#' . $attached_image->for_color === $hexCode){
                                            ?>
                                                <li>
                                                    <a class="<?php if($iteration === 0){ ?>zoomThumbActive<?php } ?>" href="javascript:void(0);" rel="{gallery: 'gal1', smallimage: '<?php echo $attached_image->image_url[0]; ?>',largeimage: '<?php echo $attached_image->guid; ?>'}">
                                                        <img src="<?php echo $attached_image->image_url[0]; ?>">
                                                    </a>
                                                </li>
                                            <?php
                                            $iteration++;
                                            }
                                        }
                                    ?>
                            </ul>
                            <script type="text/javascript">
                                $(document).ready(function(){  
                                        $('a#demo1').jqzoom({
                                                lens:true,  
                                                preloadImages: true,  
                                                alwaysOn:false,  
                                                zoomWidth: 300,  
                                                zoomHeight: 250,  
                                                xOffset:90,  
                                                yOffset:30,  
                                                position:'left',
                                                zoomType: 'standard'
                                        });  
                                });  
                            </script> 
                        </div>
			
			
			</div>
			<div class="product_right">
				<div class="nr">Gyártó: ULTIMATES AUTHENTIC Hungary.</div>
				<div class="sizes">
					Választható méretek: 
					<?php
						foreach ($sizes as $key => $size) {
							echo $size->name . (($key + 1 === sizeof($sizes)) ? '' : ', ');
						}
					?>
				</div>
				<div class="colors">
					Színvariációk: 
					<?php
						foreach ($existingColors as $key => $existingColor) {
							echo '<span style="background-color:' . $existingColor->hex . '">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;';
						}
					?>
				</div>
				<!-- div class="excerpt">
					<?php // the_excerpt(); ?>
				</div -->
				<div class="description">
					<?php the_content(); ?>
				</div>
					<?php if(is_user_logged_in()){ ?>
						<div class="prices">
                                <?php $price = get_field('price');
                                    if(intval($price) > 0){
                                ?>
                                <div class="price primary">
                                    Ár normál méretekben S-M-L-XL-XXL:  <strong><?php $price = get_field('price'); the_field('price'); ?> Ft</strong>
                                    <?php $loop = new WP_Query( array( 'post_type' => 'penznemek', 'posts_per_page' => -1 ) ); ?>
                                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <strong>(<?php echo round(intval($price) / intval(get_field('arfolyam')), 2) . ' ' . get_field('jel'); ?>)</strong>
                                    <?php endwhile; wp_reset_query(); ?>
                                </div>
                                <?php } ?>
                                <?php $price = get_field('price_2');
                                    if(intval($price) > 0){
                                ?>
                                <div class="price secondary">
                                    Ár extra méretekben 3XL-4XL-5XL-6XL: <strong><?php $price = get_field('price_2'); the_field('price_2') ?> Ft</strong>
                                    <?php $loop = new WP_Query( array( 'post_type' => 'penznemek', 'posts_per_page' => -1 ) ); ?>
                                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <strong>(<?php echo round(intval($price) / intval(get_field('arfolyam')), 2) . ' ' . get_field('jel'); ?>)</strong>
                                    <?php endwhile; wp_reset_query(); ?>
                                </div>
                                <?php } ?>
                                <?php $price = get_field('price_3');
                                    if(intval($price) > 0){
                                ?>
                                <div class="price tertiary">
                                    Ár óriás méretekbem 7XL-8XL: <strong><?php $price = get_field('price_3'); the_field('price_3') ?> Ft</strong>
                                    <?php $loop = new WP_Query( array( 'post_type' => 'penznemek', 'posts_per_page' => -1 ) ); ?>
                                    <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                                        <strong>(<?php echo round(intval($price) / intval(get_field('arfolyam')), 2) . ' ' . get_field('jel'); ?>)</strong>
                                    <?php endwhile; wp_reset_query(); ?>
                                </div>
                                <?php } ?>
                          </div>
                     <?php } ?>
                                <?php
                                    $fieldRibbon = get_field('ribbon', $post->ID);
                                    if(!empty($fieldRibbon)){
                                        ?>
                                            <span class="ribbon"><?php echo $fieldRibbon; ?></span>
                                        <?php
                                    }
                                ?>
                                <?php
                                    $fieldRibbon45 = get_field('ferde_zaszlo', $post->ID);
                                    if(!empty($fieldRibbon45)){
                                        ?>
                                            <span class="ribbon45"><?php echo $fieldRibbon45; ?></span>
                                        <?php
                                    }
                                ?>
		<?php endwhile; endif; ?>
		<?php edit_post_link('Módosítás.', '<p>', '</p>'); ?>
	</section>
</div>
<?php
get_template_part( "sidebar", "parents" );
get_footer();
?>
