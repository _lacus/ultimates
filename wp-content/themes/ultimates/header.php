<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="hu-HU">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="hu-HU">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="hu-HU">
<!--<![endif]-->
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<title><?php wp_title( '|', true, 'right' ); ?><?php bloginfo('name'); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<script type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?sensor=false">
	</script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/magnify2/js/jquery.jqzoom-core.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/page.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/js/magnify2/css/jquery.jqzoom.css">
	<link rel="stylesheet" href="wp-content/themes/ultimates/css/nivo_slider/themes/default/default.css" type="text/css" media="screen"/> <link rel="stylesheet" href="wp-content/themes/ultimates/css/nivo_slider/nivo-slider.css" type="text/css" media="screen"/>
	<!-- script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/moment.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/hu.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/combodate.js"></script>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/script.js"></script -->

</head>
<body <?php body_class(); ?>>
<div class="middle">
	<header role="banner">
		<h1 title="Szabadidő ruházat, nagyméretű ruházat, sportruházat,"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		
		<h2><?php //bloginfo('description'); ?>Ultimates ruházat-gyártás nagykereskedelem. <br>Szabadidő ruházat, sportruházat, férfi ruházat. <br>Normál és extra méretekben is,  M-8XL. <br> sportos elegáns megjelenés, minőségi kivitelben. </h2>
		<nav id="main_menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
		</nav>
		<nav id="social_menu" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'social', 'menu_class' => 'nav-menu' ) ); ?>
		</nav>
		<nav id="user_menu" role="navigation">
			<?php if(is_user_logged_in()){ ?>
				<ul>
					<li id="logout"><a href="<?php echo wp_logout_url( home_url() ); ?>" title="Kijelentkezés">kijelentkezés</a></li>
				</ul>
			<?php } else {
				wp_nav_menu( array( 'theme_location' => 'user', 'menu_class' => 'nav-menu' ) );
			} ?>
		</nav>
		<div class="login_form">
			<?php
			$wid = new login_wid;
			$wid->loginForm();
			?>
		</div>
	</header>
